//
//  Rule.swift
//  nm
//
//  Created by Pierre Ballesta on 25/08/2019.
//  Copyright © 2019 Pierre Ballesta. All rights reserved.
//

import Foundation

// Rules of Chess

// promotion of the Pawn
func Promotion(Line : Int, Colonne : Int,chessBoard : [[Pièce]]) -> [[Pièce]]{
    var board = chessBoard
    var prom : String
    print("Change your Pawn in : Queen(Q), Rook(R), " + "Bishop(B), " + "Knight(C):");
    prom = readLine() ?? "Q";
    print(prom);
    if (prom == "Q"){
        board[Colonne][Line] = Pièce(color : board[Colonne][Line].Couleur, name: PièceNames.Queen);
        board[Colonne][Line].HasMoved = true;
    }
    else if (prom == "R"){
        board[Colonne][Line] = Pièce(color : board[Colonne][Line].Couleur, name: PièceNames.Rook);
        board[Colonne][Line].HasMoved = true;
    }
    else if (prom == "B"){
        board[Colonne][Line] = Pièce(color : board[Colonne][Line].Couleur, name: PièceNames.Bishop);
        board[Colonne][Line].HasMoved = true;
    }
    else if (prom == "C"){
        board[Colonne][Line] = Pièce(color : board[Colonne][Line].Couleur, name: PièceNames.Knight);
        board[Colonne][Line].HasMoved = true;
    }
    else{
        print("Changed to : Queen.");
        board[Colonne][Line] = Pièce(color : board[Colonne][Line].Couleur, name: PièceNames.Queen);
        board[Colonne][Line].HasMoved = true;
    }
    return board
}

// Move the pieces
func MovePièce(lineStart : Int, columnStart : Int, lineEnd : Int, columnEnd : Int, chessBoard: [[Pièce]]) -> [[Pièce]]{
    var board = chessBoard
    let Temp : Pièce = board[columnStart][lineStart];
    // Change the piece in the arrival square.
    board[columnEnd][lineEnd] = Temp;
    // Remove the previous Piece.
    board[columnStart][lineStart] = Pièce();
    // Change the position inside the piece.
    // The piece has moved.
    board[columnEnd][lineEnd].HasMoved = true;
    
    if board[columnEnd][lineEnd].Name == PièceNames.Pawn{
        
        var sign = 1;
        if (chessBoard[columnStart][lineStart].Couleur == Color.White){
            sign = -1;}
        
        // Promotion ?
        if lineEnd == 7 && board[columnEnd][lineEnd].Couleur == Color.White{
            board = Promotion(Line : lineEnd, Colonne : columnEnd, chessBoard : board)}
        if lineEnd == 0 && board[columnEnd][lineEnd].Couleur == Color.Black{
            board = Promotion(Line : lineEnd, Colonne : columnEnd, chessBoard : board)}
        
        // En passant catch.
        let EnPassantL = (columnStart > 0 && chessBoard[columnStart - 1][lineStart].Enpassant && columnEnd == columnStart - 1 && lineStart == lineEnd + sign)
        let EnPassantR = (columnStart < 7 && chessBoard[columnStart + 1][lineStart].Enpassant && columnEnd == columnStart + 1 && lineStart == lineEnd + sign)
        
        // removing the other pawn
        if EnPassantL || EnPassantR {
            board[columnEnd][lineStart] = Pièce()
        }
    }
    return board
}

// Check if the squares between start and end are empty
func AreSquaresEmpty(lineStart : Int, columnStart : Int, lineEnd : Int, columnEnd : Int, chessBoard: [[Pièce]]) -> Bool{
    // direction of the displacement:
    let dc:Int = (columnStart - columnEnd).sign();
    let dl:Int = (lineStart - lineEnd).sign();
    // number of square advanced through:
    let NbrSquare = max(abs(lineStart - lineEnd),
                        abs(columnStart - columnEnd));
    // the displacement is a straight line or a diagonal and the end piece is a different color:
    let v0 = (dc*dl == 0 || abs(lineStart - lineEnd) == abs(columnStart - columnEnd));
    var v1 = v0 && NbrSquare != 0 && chessBoard[columnStart][lineStart].Couleur != chessBoard[columnEnd][lineEnd].Couleur;
    if (!v1){
        return false;}
    // Are the squares touching?
    if (v1 && NbrSquare == 1){
        return true;}
    // Are the visited squares empty?
    for j in 1...NbrSquare-1 {
        v1 = v1 && (chessBoard[columnStart + (j * dc)][lineStart + (j * dl)].Couleur == Color.Rouge);
    }
    return v1;
}

func CanPawnMoveThere(lineStart : Int, columnStart : Int, lineEnd : Int, columnEnd : Int, chessBoard: [[Pièce]]) -> Bool{
    var sign = 1;
    if (chessBoard[columnStart][lineStart].Couleur == Color.White){
        sign = -1;}
    /*
     |
     |
     ♙
     */
    // Move two cases for the first move.
    if (lineStart - lineEnd == sign * 2 && columnStart - columnEnd == 0 && chessBoard[columnStart][lineStart].HasMoved == false && chessBoard[columnEnd][lineEnd].Couleur == Color.Rouge && chessBoard[columnStart][lineStart - sign].Couleur == Color.Rouge){
        return true;}
        /*
         |
         ♙
         */
        // Move one case forward.
    else if (lineStart - lineEnd == sign && columnStart - columnEnd == 0 && chessBoard[columnEnd][lineEnd].Couleur == Color.Rouge){
        return true;}
        /*
         \ /
         ♙  only to take a piece.
         */
        // Take in diagonal
    else if (chessBoard[columnEnd][lineEnd].Couleur != chessBoard[columnStart][lineStart].Couleur && chessBoard[columnEnd][lineEnd].Couleur != Color.Rouge && lineStart - lineEnd == sign && abs(columnStart - columnEnd) == 1){
        return true;}
        /*
         ♙ |   ♙ ♟   ♙ ♟
         |           \     ♙
         ♟
         */
        // en passant
    else if (columnStart > 0 && chessBoard[columnStart - 1][lineStart].Enpassant && columnEnd == columnStart - 1 && lineStart == lineEnd + sign){
        return true;}
    else if (columnStart < 7 && chessBoard[columnStart + 1][lineStart].Enpassant && columnEnd == columnStart + 1 && lineStart == lineEnd + sign){
        return true;}
    else{
        return false;}}

func CanKnightMoveThere(lineStart : Int, columnStart : Int, lineEnd : Int, columnEnd : Int, chessBoard: [[Pièce]]) -> Bool{
    /*
     |-    -|
     |      |   |--    --|
     ♘      ♘  ♘       ♘
     */
    if (chessBoard[columnStart][lineStart].Couleur != chessBoard[columnEnd][lineEnd].Couleur && max(abs(columnStart - columnEnd), abs(lineStart - lineEnd)) == 2 && min(abs(columnStart - columnEnd), abs(lineStart - lineEnd)) == 1){
        return true;}
    else{
        return false;}
}

func CanRookMoveThere(lineStart : Int, columnStart : Int, lineEnd : Int, columnEnd : Int, chessBoard: [[Pièce]]) -> Bool{
    /*
     |
     -♖-  any number of empty squares
     |
     */
    return (lineStart - lineEnd == 0 || columnStart - columnEnd == 0) && AreSquaresEmpty(lineStart: lineStart, columnStart : columnStart, lineEnd : lineEnd, columnEnd : columnEnd, chessBoard: chessBoard);
}

func CanBishopMoveThere(lineStart : Int, columnStart : Int, lineEnd : Int, columnEnd : Int, chessBoard: [[Pièce]]) -> Bool{
    /*
     \ /
     ♗  any number of empty squares
     / \
     */
    return abs(lineStart - lineEnd) == abs(columnStart - columnEnd) && AreSquaresEmpty(lineStart: lineStart, columnStart : columnStart, lineEnd : lineEnd, columnEnd : columnEnd, chessBoard: chessBoard)
}

func CanQueenMoveThere(lineStart : Int, columnStart : Int, lineEnd : Int, columnEnd : Int, chessBoard: [[Pièce]]) -> Bool{
    /*
     \|/
     -♕- any number of empty squares
     /|\
     */
    return AreSquaresEmpty(lineStart: lineStart, columnStart : columnStart, lineEnd : lineEnd, columnEnd : columnEnd, chessBoard: chessBoard)
}

func CanKingMoveThere(lineStart : Int, columnStart : Int, lineEnd : Int, columnEnd : Int, chessBoard: [[Pièce]]) -> Bool{
    /*
     \|/
     -♔- only one square at a time
     /|\
     */
    return chessBoard[columnStart][lineStart].Couleur != chessBoard[columnEnd][lineEnd].Couleur && max(abs(columnStart - columnEnd), abs(lineStart - lineEnd)) == 1
}


func CanPièceMoveThere(lineStart : Int, columnStart : Int, lineEnd : Int, columnEnd : Int, chessBoard: [[Pièce]]) -> Bool{
    if chessBoard[columnStart][lineStart].Name == PièceNames.Pawn {
        let a = CanPawnMoveThere(lineStart: lineStart, columnStart : columnStart, lineEnd : lineEnd, columnEnd : columnEnd, chessBoard: chessBoard)
        return a
    }
    else if chessBoard[columnStart][lineStart].Name == PièceNames.Knight {
        let a = CanKnightMoveThere(lineStart: lineStart, columnStart : columnStart, lineEnd : lineEnd, columnEnd : columnEnd, chessBoard: chessBoard)
        return a
    }
    else if chessBoard[columnStart][lineStart].Name == PièceNames.Rook {
        let a = CanRookMoveThere(lineStart: lineStart, columnStart : columnStart, lineEnd : lineEnd, columnEnd : columnEnd, chessBoard: chessBoard)
        return a
    }
    else if chessBoard[columnStart][lineStart].Name == PièceNames.Bishop {
        let a = CanBishopMoveThere(lineStart: lineStart, columnStart : columnStart, lineEnd : lineEnd, columnEnd : columnEnd, chessBoard: chessBoard)
        return a
    }
    else if chessBoard[columnStart][lineStart].Name == PièceNames.Queen {
        let a = CanQueenMoveThere(lineStart: lineStart, columnStart : columnStart, lineEnd : lineEnd, columnEnd : columnEnd, chessBoard: chessBoard)
        return a
    }
    else if chessBoard[columnStart][lineStart].Name == PièceNames.King {
        let a = CanKingMoveThere(lineStart: lineStart, columnStart : columnStart, lineEnd : lineEnd, columnEnd : columnEnd, chessBoard: chessBoard)
        return a
    }
    else {return false}
};

func IsInCheck(chessBoard: [[Pièce]], color : Color) -> Bool{
    var inCheck = false
    var i : Int = 0
    var j : Int = 0
    var n : Int = 0
    while (chessBoard[i][j].Name != PièceNames.King || chessBoard[i][j].Couleur != color) && (n<boardSize*boardSize){
        n += 1;
        i = n/boardSize;
        j = n%boardSize;
    }
    let a = i;
    let b = j;
    for i in 0...boardSize-1{
        for j in 0...boardSize-1{
            inCheck = inCheck || chessBoard[i][j].Couleur != color && CanPièceMoveThere(lineStart: j, columnStart: i, lineEnd: b, columnEnd: a, chessBoard: chessBoard);
        }
    }
    return inCheck
}

func Roque(chessBoard: [[Pièce]], grand: Bool, round : Round) -> TestRoque{
    var results = TestRoque();
    var board = chessBoard;
    var i = 0;
    //direction displacement
    var dd = 1;
    // displacement Rook
    var dt = -2;
    // position of the Rook
    var pt = 7;
    // line of the king
    if (round.Couleur == Color.Black){
        i = 7;}
    // Can you castle?
    var v1 : Bool = true
    // The Piece is a King of the good color that hasn't move yet
    v1 = v1 && (board[4][i].Name == PièceNames.King && board[4][i].Couleur == round.Couleur && !board[4][i].HasMoved)
    // The rook is of the good color and hasn't move yet
    if (grand){
        dd = -1;
        dt = 3;
        pt = 0;
        //v1 = v1 && chessBoard[pt - 3 * dd][i].Couleur == Color.Rouge;
    }
    v1 = v1 && (board[pt][i].Name == PièceNames.Rook && board[pt][i].Couleur == round.Couleur && !board[pt][i].HasMoved)
    // The squares in between are empty
    for j in 1...abs(dt){
        v1 = v1 && board[4 + dd*j][i].Name == PièceNames.Empty;
    }
    // The king is not in check
    v1 = v1 && !IsInCheck(chessBoard: board, color : round.Couleur)
    // During its displacement the king does not end in check
    for j in 1...abs(dt){
        board = MovePièce(lineStart : i, columnStart : 4, lineEnd : i, columnEnd : 4 + dd*j, chessBoard: board)
        v1 = v1 && !IsInCheck(chessBoard: board, color : round.Couleur)
        board = chessBoard;
    }
    
    results.CanRoque = v1;
    
    if v1{
        board = MovePièce(lineStart : i, columnStart : 4, lineEnd : i, columnEnd : 4 + dd*2, chessBoard: board)
        board = MovePièce(lineStart : i, columnStart : pt, lineEnd : i, columnEnd : 4 + dt, chessBoard: board)
        results.ChessBoard = board;
    }
    else {
        print ("You cannot castle from this position.")
        results.ChessBoard = chessBoard
    }
    return results
}
