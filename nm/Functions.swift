//
//  Functions.swift
//  nm
//
//  Created by Pierre Ballesta on 25/08/2019.
//  Copyright © 2019 Pierre Ballesta. All rights reserved.
//

import Foundation

// If the EnPassant rule was not used we reinitialise the values.
func InitEnPassant(round : Round, chessBoard : [[Pièce]]) -> [[Pièce]]{
    var board = chessBoard;
    
    for i in 0...boardSize-1{
        for j in 0...boardSize-1{
            if (board[i][j].Couleur == round.Couleur){
                board[i][j].Enpassant = false;
            }
        }
    }
    return board
}

// We change the color of the player whose turn it is and increment the turn number.
func NewTurn(round : Round) -> Round{
    var tour = round;
    tour.Nbr+=1;
    if (tour.Couleur == Color.White){
        tour.Couleur = Color.Black;}
    else if (tour.Couleur == Color.Black){
        tour.Couleur = Color.White;}
    return tour;
}

// This is used to read the players input.
func findNumber(string : String, number : Int) -> Int{
    var numRet:Int?
    let index = string.index(string.startIndex, offsetBy: number)
    numRet = Int(String(string[index]))
    return numRet ?? -1
}

// The rules are checked here.
func GiveYourMove(round: Round, chessBoard: [[Pièce]]) -> [[Pièce]] {
    var board = chessBoard
    var displacement : String;
    var startLine:Int = -1;
    var arrivalLine:Int = -1;
    var startColumn:Int = -1;
    var arrivalColumn:Int = -1;
    
    var result = TestRoque();
    var validCases = false;
    var validMove = false;
    var validColor = false;
    let validOrder = false;
    // lire la console
    while (validOrder == false){
        print(round.Couleur, " turn, give your movement (line,column)" + "->(line,column):");
        displacement = readLine() ?? " ";
        // exit the loop.
        if (displacement == "break"){
            //goto Problem;
        }
            // castling short code
        else if (displacement == "petit roque"){
            result = Roque(chessBoard : board, grand : false, round : round);
            if (result.CanRoque){
                return result.ChessBoard;}
            else{
                continue;}
        }
            // castling long code
        else if (displacement == "grand roque"){
            result = Roque(chessBoard : board, grand : true, round : round);
            if (result.CanRoque){
                return result.ChessBoard;}
            else{
                continue;}
        }
            // other moves
        else if (displacement.count == 12){
            // read the beginning and ending positions
            startLine = findNumber(string: displacement, number: 1)-1
            startColumn = findNumber(string: displacement, number: 3)-1
            arrivalLine = findNumber(string: displacement, number: 8)-1
            arrivalColumn = findNumber(string: displacement, number: 10)-1
            let v1 = (Validité(i: startLine) && Validité(i: arrivalLine) && Validité(i: startColumn) && Validité(i: arrivalColumn));
            if (v1 == false){
                print("lines and columns are integers " + "between 1 and 8.\n");
                continue;
            }
            else{
                validCases = true;}
        }
        else{
            print(displacement);
            print("Respect formating.\n");
            continue;
        }
        // The piece in the beginning position is the same color
        //as the turn played.
        validColor = false || board[startColumn][startLine].Couleur == round.Couleur;
        if (!validColor){
            print("Don't move your opponent pieces.\n");
            continue;
        }
        // The move is a valid move for the chosed piece.
        validMove = CanPièceMoveThere(lineStart: startLine, columnStart: startColumn, lineEnd: arrivalLine, columnEnd: arrivalColumn, chessBoard: board);
        if (!validMove){
            print("Respect chess rules!\n");
            continue;
        }
        // If everything else is valid, we check for our king being
        //in check at the end of the move.
        
        //Other moves.
        if (validMove && validColor && validCases){
            board = MovePièce(lineStart : startLine, columnStart : startColumn, lineEnd : arrivalLine, columnEnd : arrivalColumn, chessBoard: board);
            // Check that the king is not in check.
            let isCheck = IsInCheck(chessBoard: board, color : round.Couleur);
            if (isCheck){
                print("This move ends with the king in " + "check.\n");
                board = chessBoard
                continue;
            }
            else{
                return board
            }
        }
        else{continue;}
    }
}
