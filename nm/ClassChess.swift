//
//  ClassChess.swift
//  nm
//
//  Created by Pierre Ballesta on 25/08/2019.
//  Copyright © 2019 Pierre Ballesta. All rights reserved.
//

import Foundation

// Define the colors that the pieces can take.
// We added Rouge (red) for empty cases.
enum Color{
    case White, Black, Rouge;
}

enum PièceNames{
    case Empty, Pawn, Knight, Bishop, Rook, Queen, King
}

let PièceDict=[PièceNames.Empty:("_","_","_"), PièceNames.Pawn:("♙","\u{265F}","_"), PièceNames.Knight:("♘","♞","_"), PièceNames.Bishop:("♗","♝","_"), PièceNames.Rook:("♖","♜","_"), PièceNames.Queen:("♕","♛","_"), PièceNames.King:("♔","♚","_")]

// During the test for the Castle, we need a boolean and a board.
struct TestRoque{
    var CanRoque : Bool
    var ChessBoard : [[Pièce]]
    
    init(){
        self.CanRoque = false
        self.ChessBoard = [[Pièce(),Pièce()],[Pièce(),Pièce()]];
    }
    
    init(ChessBoard : [[Pièce]]){
        self.CanRoque = false
        self.ChessBoard = ChessBoard;
    }
}

// Define the round with color and number.
struct Round{
    var Couleur : Color
    var Nbr : Int
    
    init(){
        Couleur = Color.White;
        Nbr = 1;
    }
}

// Different king of pieces.
struct Pièce {
    var Couleur : Color
    var Symbol : String
    var Name : PièceNames
    
    init(color : Color, name : PièceNames){
        Name = name
        Couleur = color;
        if Couleur == Color.White{Symbol=String(PièceDict[name]?.0 ?? "?");}
        else if Couleur == Color.Black{Symbol=String(PièceDict[name]?.1 ?? "?");}
        else {Symbol=String(PièceDict[name]?.2 ?? "?");}
    }
    
    init(){
        Name = PièceNames.Empty;
        Couleur = Color.Rouge;
        Symbol="_";
    }
    
    var HasMoved : Bool = false;
    var Enpassant : Bool = false;
}
