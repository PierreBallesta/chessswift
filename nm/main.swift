//
//  main.swift
//  nm
//
//  Created by Pierre Ballesta on 24/08/2019.
//  Copyright © 2019 Pierre Ballesta. All rights reserved.
//

import Foundation

extension Int {
    func sign() -> Int {
        if self==0{
            return 0}
        return (self < 0 ? -1 : 1)
    }
    /* or, use signature: func sign() -> Self */
}

extension Float {
    func sign() -> Int {
        if self==0.0{
            return 0}
        return (self < 0.0 ? -1 : 1)
    }
}

// Initialisation
var isCheckMate = false;
var board : [[Pièce]] =  boardCreation(nLine: boardSize, nColumn: boardSize);
var tour = Round();

// filling the chess board.

for i in 2...5{
    for j in 0...boardSize-1 {
        board[j][i] = Pièce();
        board[j][i] = Pièce();
    }
}
for j in 0...boardSize-1 {
    board[j][1] = Pièce(color: Color.White, name: PièceNames.Pawn);
    board[j][6] = Pièce(color: Color.Black, name: PièceNames.Pawn);
}
board[0][0] = Pièce(color: Color.White, name: PièceNames.Rook);
board[7][0] = Pièce(color: Color.White, name: PièceNames.Rook);
board[0][7] = Pièce(color: Color.Black, name: PièceNames.Rook);
board[7][7] = Pièce(color: Color.Black, name: PièceNames.Rook);
board[1][0] = Pièce(color: Color.White, name: PièceNames.Knight);
board[6][0] = Pièce(color: Color.White, name: PièceNames.Knight);
board[1][7] = Pièce(color: Color.Black, name: PièceNames.Knight);
board[6][7] = Pièce(color: Color.Black, name: PièceNames.Knight);
board[2][0] = Pièce(color: Color.White, name: PièceNames.Bishop);
board[5][0] = Pièce(color: Color.White, name: PièceNames.Bishop);
board[2][7] = Pièce(color: Color.Black, name: PièceNames.Bishop);
board[5][7] = Pièce(color: Color.Black, name: PièceNames.Bishop);
board[3][0] = Pièce(color: Color.White, name: PièceNames.Queen);
board[4][0] = Pièce(color: Color.White, name: PièceNames.King);
board[3][7] = Pièce(color: Color.Black, name: PièceNames.Queen);
board[4][7] = Pièce(color: Color.Black, name: PièceNames.King);

while (isCheckMate == false){
    DrawBoard(chessBoard: board);
    board = GiveYourMove(round: tour, chessBoard: board);
    // Is the ennemy king in check?
    var coloOp = Color.Rouge
    if tour.Couleur == Color.White {coloOp = Color.Black}
    if tour.Couleur == Color.Black {coloOp = Color.White}
    let inCheck = IsInCheck(chessBoard: board, color: coloOp);
    if (inCheck == true){
        print("Échec au roi.\n");
    }
    // Next turn
    tour = NewTurn(round: tour);
    board = InitEnPassant(round : tour, chessBoard : board)
}



