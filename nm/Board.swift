//
//  Board.swift
//  nm
//
//  Created by Pierre Ballesta on 25/08/2019.
//  Copyright © 2019 Pierre Ballesta. All rights reserved.
//

import Foundation

let boardSize=8

// Check that ligns and columns are in the good interval.
func Validité(i: Int) -> Bool{
    if (i > -1 && i < boardSize){
        return true;}
    else{
        return false;}
}

// The Board is a 2D array
func boardCreation(nLine: Int, nColumn: Int) -> [[Pièce]]{
    var matrix: [[Pièce]] = []
    for i in 0...nLine-1 {
        matrix.append( [] )
        
        for _ in 0...nColumn-1 {
            matrix[i].append(Pièce())
        }
    }
    return matrix
}

// Drawing the board.
func DrawBoard(chessBoard : [[Pièce]]){
    print("");
    var line : String = " |";
    for j in 0...boardSize-1{
        line = line + String(j + 1);
        line = line + "|";
    }
    print(line)
    for i in 0...boardSize-1{
        line = String(i+1) + "|";
        for j in 0...boardSize-1{
            line = line + chessBoard[j][i].Symbol;
            line = line + "|";
        }
        print(line);
    }
}

