# ChessSwift

This is a transcription in Swift of my chess plateau program for C#.  
It's actually more of a 0.1 version as the code is reduced and simplified.

The commands remain the same as its previous version.


## Authors

* **Pierre Ballesta** -  [PierreBallesta](https://bitbucket.org/PierreBallesta)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.